import java.io.*;
import java.util.*;

//Risako Owan and Brynna Mering

public class newApriori{
    //Given by user
    private static int support;
    private static double confidence;

    //Set of movies seen by each user
    private static List<Integer[]> transactionList = new ArrayList<Integer[]>();

    //Stores the frequency of each movie individually
    private static HashMap<String, Integer> movieFrequencyDict = new HashMap<String, Integer>();

    //Stores the actual movie names for each ID
    private static HashMap<String, String> movieNames = new HashMap<String, String>();
    
    //Stores all frequent Itemsets of all sizes
    private static List<Integer[]> allFrequentItemSets = new ArrayList<Integer[]>();

    private static HashMap<Integer[], Integer> frequentItemsetToFrequency = new HashMap<Integer[], Integer>();

    //Keeps track of the largest transaction in the transaction list
    //Used for determining the size limit for k
    private static int largestTransactionSize;
    
    //Reads file and adds set of seen movies for each user to listOfSeenMoviesForEachUser
    //In the same iteration, adds a dictionary for movie frequency {movie ID : freq}
    public static void readUserData() {
        try {
            File file = new File("ratings.dat");
            Scanner scanner = new Scanner(file);
            //Assumes scanner has at least one line
            String[] fileLine = scanner.nextLine().split("::");
            //List of MovieIDs to be passed into transactionList. Will be reset for each user.
            List<Integer> userMovies = new ArrayList<Integer>();
            userMovies.add(Integer.parseInt(fileLine[1]));

            String currentUser = fileLine[0];
            //While nextLine isn't null

            while (scanner.hasNextLine()) {



                fileLine = scanner.nextLine().split("::");
                //Adding movies to each user's set of viewed movies
                //If we are at the next user
                if (!currentUser.equals(fileLine[0])) {
                    if (userMovies.size() > largestTransactionSize) {
                        largestTransactionSize = userMovies.size();
                    }
                    Integer[] userMoviesArray = new Integer[userMovies.size()];
                    userMoviesArray = userMovies.toArray(userMoviesArray);
                    Arrays.sort(userMoviesArray);
                    transactionList.add(userMoviesArray);
                    userMovies = new ArrayList<Integer>();
                    currentUser = fileLine[0];
                }
                userMovies.add(Integer.parseInt(fileLine[1]));
                //Creates a dictionary the stores the frequency of each movie
                if(movieFrequencyDict.containsKey(fileLine[1])){
                    int old = movieFrequencyDict.get(fileLine[1]);
                    movieFrequencyDict.put(fileLine[1], old + 1);
                }else{
                    movieFrequencyDict.put(fileLine[1], 1);
                }
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Reads in data from movies.dat and creates dictionary {ID: movie names}
    public static void readMovieNames(){
        try {
            File file = new File("movies.dat");
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String[] fileLine;
            String tempFileLine = reader.readLine();
            while (tempFileLine != null) {

                fileLine = tempFileLine.split("::");
                movieNames.put(fileLine[0], fileLine[1]);
                tempFileLine = reader.readLine();
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Iterates through movieFrequencyDict and creates a list of frequent individual movies
    //adds these frequent itemsets to allFrequentItemSets
    public static List<Integer> getFrequentSingles(){
        List<Integer> frequentSingles = new ArrayList<Integer>();
        for(String key : movieFrequencyDict.keySet()){
            if(movieFrequencyDict.get(key)>=support){
                Integer[] newCandidateArray = new Integer[1];
                newCandidateArray[0] = Integer.parseInt(key);
                //Adds Integers to frequentSingles, and Integer[] to allFrequentItemSets
                frequentSingles.add(newCandidateArray[0]);
                allFrequentItemSets.add(newCandidateArray);
            }
        }
        Collections.sort(frequentSingles);
        return frequentSingles;
    }

    //Creates a list of arrays of frequent pairs of movies from the given list of frequent 
    //single movies. Also, adds these frequent itemsets to allFrequentItemSets.
    public static List<Integer[]> getFrequentPairs(List<Integer> frequentSingles){
        List<Integer[]> frequentPairs = new ArrayList<Integer[]>();
        //Creates a list of non-duplicate pairs
        for (int i = 0; i < frequentSingles.size(); i++) {
            for (int j = i + 1; j < frequentSingles.size(); j++) {
                Integer[] newPair = new Integer[2];
                newPair[0] = frequentSingles.get(i);
                newPair[1] = frequentSingles.get(j);
                frequentPairs.add(newPair);
            }
        }

        // for (int i = 0; i < frequentSingles.size(); i++) {
        //     System.out.println("weird..."+i);
        //     for (int j = i + 1; j < frequentSingles.size(); j++) {
        //         if (i != j) {
        //             Integer[] newPair = new Integer[2];
        //             newPair[0] = frequentSingles.get(i)[0];
        //             newPair[1] = frequentSingles.get(j)[0];
        //             Arrays.sort(newPair);
        //             if (!(frequentPairs.contains(newPair))) {
        //                 frequentPairs.add(newPair);
        //             }
        //         }  
        //     }
        // }
        return frequentPairs;
    }

    //This function recursively generates sub HashTrees for each element in the transaction 
    //The length of frequentItem must be at least 2, frequentItem
    @SuppressWarnings("unchecked")
    public static HashMap<Integer, ? extends Object> recursiveHashTree(Integer[] frequentItem) {
        //base case
        if (frequentItem.length == 1) {
            //HashMap<Integer,Integer> lastDict = new HashMap<Integer,Integer>();  
            HashMap<Integer, Integer> lastDict = new HashMap<Integer, Integer>();
            lastDict.put(frequentItem[0], 0);
            //HashMap<Integer,HashMap> dict1 = new HashMap<Integer,HashMap>();
            //HashMap dict1 = new HashMap();
            //dict1.put(frequentItem[0],lastDict);
            return lastDict;
        //recursive case
        } else {
            //HashMap<Integer,HashMap> dict2 = new HashMap<Integer,HashMap>();
            HashMap<Integer, HashMap> dict2 = new HashMap<Integer, HashMap>();
            Integer[] newArray = makeSubarrayCopy(frequentItem);
            dict2.put(frequentItem[0], recursiveHashTree(newArray));
            return dict2;
        }
    }

    //
    public static Integer[] makeSubarrayCopy(Integer[] originalArray) {
        Integer[] temp = new Integer[originalArray.length - 1];
        for (int i = 1; i < originalArray.length; i++) {
            //Assumes no nulls
            temp[i-1] = originalArray[i];
        }
        return temp;
    }


    // public static List<Integer[]> createTripletCandidates(List<Integer[]> frequentPairs){
    //     List<Integer[]> tripletCandidates = new ArrayList<Integer[]>();

    //     int listSize = frequentPairs.size();
    //     System.out.println(listSize);
    //     for(int i = 0; i < listSize; i++){
    //         //System.out.println("42642 <- " + i);

    //         for(int j = 0; j < listSize; j++){
    //             if (i != j) {
    //                 Integer[] newCandidate = generateCandidates(frequentPairs.get(i), frequentPairs.get(j));
                    
    //                 if (newCandidate != null) {
    //                     Arrays.sort(newCandidate);

    //                     if(!tripletCandidates.contains(newCandidate)){
    //                         tripletCandidates.add(newCandidate);
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     //System.out.println("got to here");

    //     return tripletCandidates;
    // }//end of createTripletCandidates


    public static HashMap<Integer, HashMap> createHashTree(List<Integer[]> possibleCandidates) {

        //Creates hash tree with initial count 0
        HashMap<Integer, HashMap> hashTree = new HashMap<Integer, HashMap>();
        for (Integer[] pc : possibleCandidates) {
            Integer[] newArray = makeSubarrayCopy(pc);
            hashTree.put(pc[0], recursiveHashTree(newArray));
        }

        return hashTree;
    }


    //Generates candidates for two itemsets by finding all sets of size (set1.size() + 1)
    //that share the first (set1.size() - 1) items
    public static Integer[] generateCandidates(Integer[] itemset1, Integer[] itemset2) {
        int k = itemset1.length + 1;
        Integer[] newCandidate = null;

        Integer[] firstPartOfItemset1 = Arrays.copyOfRange(itemset1,0,k-2);
        Integer[] firstPartOfItemset2 = Arrays.copyOfRange(itemset2,0,k-2);

        //If every element in the first k-2 indices of itemset1 and itemset2 are the same 
        //and the items in the very last indices are different
        if (Arrays.equals(firstPartOfItemset1, firstPartOfItemset2) && !(itemset1[k-2].equals(itemset2[k-2]))) {
            newCandidate = Arrays.copyOf(itemset1, k);
            System.arraycopy(itemset2,k-2,newCandidate,k-1,1);
        }
        return newCandidate;
    }//End generateCandidates

    //Implements Fn-1xFn-1 to generate all frequent itemsets
    @SuppressWarnings("unchecked")
    public static void generateAllCandidates() {
        //Creates frequent singles and adds them to allFrequentItemSets
        List<Integer> frequentSingleton = getFrequentSingles();
        System.out.println("finished getting singletons");
        //Use hash tree data structure from k-1 = 3
        int currentK = 3;
        int currentKMinus1 = currentK - 1;
        //Creates candidate triplets
        List<Integer[]> kMinus1ItemSets = getFrequentPairs(frequentSingleton);//createTripletCandidates(frequentPairs);
        System.out.println("finished getting frequent pairs");
        HashMap<Integer, HashMap> currentHashTree;
        List<Integer[]> sizeKItemSets = new ArrayList<Integer[]>();

        while (currentK <= 100) {//largestTransactionSize

            //Build hash-tree with kMinus1ItemSets (candidates, not necessarily frequent)
            //System.out.println("creating hash tree");
            currentHashTree = createHashTree(kMinus1ItemSets);

            //Goes through each transactions and counts each candidate
            for (Integer[] transaction : transactionList) {

                //Assuming transaction is a sorted array
                int transactionIndex = 0;

                //Moves to the first index of the transaction that is in a candidate
                while (!currentHashTree.containsKey(transaction[transactionIndex]) && transactionIndex < transaction.length - 1) {
                    transactionIndex++;
                }

                //Enters hash tree
                HashMap currentDict = currentHashTree.get(transaction[transactionIndex]);
                
                //Only enter if transaction might contain candidiate
                if (currentDict != null) {

                    //Iterates down the hash tree
                    //Exits when candidate is not in transaction or if it reached the second to last dictionary
                    boolean valid = true;
                    while (valid && currentDict != null) {//transactionIndex < currentKMinus1 - 1
                        transactionIndex++;
                        //The candidate elements may not necessarily be in consequetive order
                        while (transactionIndex < transaction.length - 1 && !currentDict.containsKey(transaction[transactionIndex])) {
                            transactionIndex++;
                        }

                        try {
//currentDict.getClass().equals(HashMap.class) && 
                            // if (currentDict.containsKey(transaction[transactionIndex])) && !currentDict.get(transaction[transactionIndex]).getClass().equals(Integer.class)) {
                            //     System.out.println(currentDict.get(transaction[transactionIndex]).getClass());
                            //if (currentDict.get(transaction[transactionIndex]))
                            currentDict = (HashMap)currentDict.get(transaction[transactionIndex]);
                            // } else {
                            //     valid = false;
                            // }
                        } catch (Exception e) {
                            //System.out.println(currentDict);
                            valid = false;
                        }

                    }

                    //Exits if candidate was not found (currentDict == null) or if we reached the end of hash tree
                    //If we reached the end of the hash tree, increments count
                    if (currentDict != null) {
                        Integer newCount = (Integer)currentDict.get(transaction[transactionIndex]) + 1;
                        currentDict.put(transaction[transactionIndex], newCount);
                    }
                }
            }//end of for-loop

            //Now, we're done counting all the frequencies of the candidates
            //Perform depth first search using list as a stack
            //Adds result to global variable: frequentItemsetToFrequency
            depthFirstSearch(currentHashTree);
                    // for (Integer[] key : depthFirstSearch(currentHashTree).keySet()) {
                    //     if (!frequentItemsetToFrequency.containsKey(key)) {
                    //         frequentItemsetToFrequency.put(key, frequentItemsetToFrequency.get(key));
                    //     }
                    // }
            //frequentItemsetToFrequency.putAll(depthFirstSearch(currentHashTree));

                                    //System.out.println("333333333333");

    //             }
    //         }


    //         //Creating candidates of size k by calling generate candidates
    //         for(int i = 0; i < currentKMinus1; i++){
    //             for(int j = 0; j < currentKMinus1; j++){
    //                 if(i != j){
       //                  Integer[] newCandidate = generateCandidates(kMinus1ItemSets.get(i), kMinus1ItemSets.get(j));
       //                  if (newCandidate != null) {
       //                      Arrays.sort(newCandidate);
       //                      if(!sizeKItemSets.contains(newCandidate)){
       //                          sizeKItemSets.add(newCandidate);
       //                      }
       //                  }
       //              }


            //System.out.println(currentHashTree);
            currentK++;
            currentKMinus1++;

    //         //take count in hash
    //         //check with original
    //         if (currentKMinus1 != 10) {
    //             allFrequentItemSets.addAll(sizeKItemSets);
    //             kMinus1ItemSets.clear();
    //             kMinus1ItemSets.addAll(sizeKItemSets);
    //             sizeKItemSets = new ArrayList<Integer[]>();
    //         }
            
        

    //         //Call method to put into hash tree
    //         //Call method to count and get all positives
    //         //Call method to check: think about chunk size here
            System.out.println(currentK);
        }//end of while loop

        for (Integer[] key : frequentItemsetToFrequency.keySet()) {
            System.out.println(key +"="+Arrays.toString(key) + " --> " + frequentItemsetToFrequency.get(key));
        }

    //     System.out.println("done with while-loop");

    //     for (int i = 0; i < sizeKItemSets.size(); i++) {
    //         System.out.println(Arrays.toString(sizeKItemSets.get(i)));
    //     }
        

    //     //After while statement
    //     //Create rules in here or different method
    //     System.out.println("doneeeee");
    }

    public static void depthFirstSearch(HashMap<Integer, HashMap> currentHashTree) {
        //Dictionary that maps from frequent candidates to frequency
        //HashMap<Integer[], Integer> frequentCandidates = new HashMap<Integer[], Integer>();
        List<Integer> pathTilHere = new ArrayList<Integer>();
        recursiveDepthFirstSearch(currentHashTree, pathTilHere);
        //HashMap<Integer[], Integer> result = recursiveDepthFirstSearch(frequentCandidates, currentHashTree, pathTilHere);
        //System.out.println(result.values());
        //return (result);

    }

    //http://stackoverflow.com/questions/1115230/casting-object-array-to-integer-array-error
    @SuppressWarnings("unchecked")
    public static void recursiveDepthFirstSearch(HashMap currentHashTree, List<Integer> pathTilHere) {

        try {
            //Base Case
            //If at the end of the tree, checks if the values are all integers
            //if (((Collection<Integer>)currentHashTree.values()).size() >= 0) {
            for (Integer key : (Set<Integer>)currentHashTree.keySet()) {
                //If the candidate is frequent
                Integer frequency = (Integer)currentHashTree.get(key);
                if (frequency >= support) {
                    pathTilHere.add(key);
                    Integer[] finalpathTilHere = Arrays.copyOf(pathTilHere.toArray(), pathTilHere.size(), Integer[].class);
                    //Integer[] finalpathTilHere = (Integer[])pathTilHere.toArray();
                    frequentItemsetToFrequency.put(finalpathTilHere, frequency);
                    
                }

            }
            //}
            
        } catch (Exception e) {
            //Recursive Case
            for (Integer key : (Set<Integer>)currentHashTree.keySet()) {
                List<Integer> newPathTilHere = pathTilHere;
                newPathTilHere.add(key);
                //frequentCandidates.putAll(recursiveDepthFirstSearch(frequentCandidates, (HashMap)currentHashTree.get(key), newPathTilHere));
                recursiveDepthFirstSearch((HashMap)currentHashTree.get(key), newPathTilHere);

            }
        }

        //return frequentCandidates;
    }

    public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            //Prompts user for program details.
            //System.out.println("Type in support threshold (for count).");
            //support = Integer.parseInt(reader.readLine());
            support = 10;
            reader.close();

            //For part II
            // System.out.println("Type in confidence threshold.");
            // confidence = Integer.parseInt(reader.readLine());

            readUserData();
            readMovieNames();
            // totalTransaction = listOfSeenMoviesForEachUser.size();
            // int desiredFreq = support * totalTransaction;
            //getFrequentItemSets(support);
            // Integer[] even = new Integer[4];
            // even[0] = 0;
            // even[1] = 2;
            // even[2] = 4;
            // even[3] = 6;
            // Integer[] even1 = new Integer[4];
            // even1[0] = 0;
            // even1[1] = 2;
            // even1[2] = 4;
            // even1[3] = 8;
            // System.out.println(Arrays.toString(generateCandidates(even, even1)));
            // generateAllCandidates();
            
            // for (int i = 0; i < frequentPairs.size(); i++) {
            //     System.out.println(Arrays.toString(frequentPairs.get(i)));
            // }
            // Integer[] test = {1,2,3,4,5};
            // Integer[] trans = {0,1,2,3,4,5,6,7};
            // Integer[] trans2 = {1,2,3,4,5,6,7,8};
            // HashMap testDict = (recursiveHashTree(test));
            // System.out.println(testDict);
            // int transactionIndex = 0;

            // // HashMap a = (HashMap)testDict.get(trans2[0]);
            // // System.out.println(a);
            // // System.out.println(a.getClass());
            // // HashMap b = (HashMap)a.get(trans2[1]);
            // // System.out.println(b);
            // // HashMap c = (HashMap)b.get(trans2[2]);
            // // System.out.println(c);
            // // HashMap d = (HashMap)c.get(trans2[3]);
            // // System.out.println(d);
            // while ((testDict.containsKey(trans2[transactionIndex]) && (transactionIndex < test.length - 1))) {
            //     System.out.println(testDict);
            //     testDict = (HashMap)testDict.get(trans2[transactionIndex]);
            //     transactionIndex++;
            //     System.out.println(transactionIndex);
            // }
            // System.out.println("exited");
            // System.out.println(testDict);
            // for (int i : testDict.keySet()) {
            //     while (!testDict.get(i).getClass().equals(int.class)) {
            //         System.out.println("key: " + i + ", val: "+testDict.get(i));
            //     }
            // }
            generateAllCandidates();



        } catch (IOException e) {
            e.printStackTrace();
        }

        
    }//end of main

}