//Use list of arrays
//change all set to arrays

import java.io.*;
import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;

//Risako Owan and Brynna Mering

public class Apriori2 {
	private static int support;
	private static int confidence;
    //keep as set
	private static List<Set<String>> listOfSeenMoviesForEachUser = new ArrayList<Set<String>>();
	private static int totalTransaction;
    //dictionary for freq size 1 candidates
    private static HashMap<String, Integer> candidateFreqDict = new HashMap<String, Integer>();
    //Integer[] used to be Set<String>: each set of seen movies/transaction
    private static HashMap<Integer[], Integer> frequentCandidateDict = new HashMap<Integer[], Integer>();
    private static HashMap<String, String> movieNames = new HashMap<String, String>();
    private static Set<Integer> frequentSingleton = new HashSet<Integer>();
    private static List<Integer[]> frequentPairs = new ArrayList<Integer[]>();
    private static List<Integer[]> allFrequentCandidates = new ArrayList<Integer[]>();
    private static HashMap<String, HashMap> hashTree = new HashMap<String, HashMap>();
    private static int largestTransactionSize = 0;


	//Reads file and adds set of seen movies for each user to listOfSeenMoviesForEachUser
    //In the same iteration, adds a dictionary for movie frequency {movie ID : freq}
	public static void readUserData() {
		try {
            File file = new File("ratings.dat");
            Scanner scanner = new Scanner(file);

            //Assumes scanner has at least one line
            String[] fileLine = scanner.nextLine().split("::");

            //Set of strings or set of integers?
            Set<String> movieSet = new HashSet<String>();
            movieSet.add(fileLine[1]);

            String currentUser = fileLine[0];

            
            //While nextLine isn't null
            while (scanner.hasNextLine()) {
            	
            	fileLine = scanner.nextLine().split("::");

            	//Adding movies to each user's set of viewed movies
                //If we are at the next user
            	if (!currentUser.equals(fileLine[0])) {
                    
                    if (movieSet.size() > largestTransactionSize) {
                        largestTransactionSize = movieSet.size();

                    }

                    //Array or list?????
            		listOfSeenMoviesForEachUser.add(movieSet);
            		movieSet = new HashSet<String>();
            		currentUser = fileLine[0];
            	}
            	
            	movieSet.add(fileLine[1]);

                
                //Adding to dictionary of movie frequency
                //Helps create frequent candidates of size one
                if(candidateFreqDict.containsKey(fileLine[1])){
                    int old = candidateFreqDict.get(fileLine[1]);
                    candidateFreqDict.put(fileLine[1], old + 1);
                }else{
                    candidateFreqDict.put(fileLine[1], 1);
                }
            }

            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
	}

    //Reads in data from movies.dat and creates dictionary {ID: movie names}
    public static void readMovieNames(){

        try {
            File file = new File("movies.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String[] fileLine;
            String tempFileLine = reader.readLine();
            while (tempFileLine != null) {
                fileLine = tempFileLine.split("::");
                movieNames.put(fileLine[0], fileLine[1]);
                tempFileLine = reader.readLine();
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // //Iterates through candidateFreqDict and creates a set (freqMovies) of frequent movies 
    // //according to the given support threshold
    // public static void getFrequentItemSets(Integer support){

    //     for(String key : candidateFreqDict.keySet()){
    //         if(candidateFreqDict.get(key)>=support){
    //             freqMovies.add(key);
    //         }
    //     }
    // }

    //Iterates through candidateFreqDict and creates a set (frequentSingleton) of freq. singletons 
    //according to the given support threshold
    public static void getFrequentSingletons(Integer support){

        for(String key : candidateFreqDict.keySet()){
            if(candidateFreqDict.get(key)>=support){
                frequentSingleton.add(Integer.parseInt(key));
            }
        }
    }

    //Iterates through candidateFreqDict and creates a list of arrays of freq. pairs 
    //according to the given support threshold
    public static void getFrequentPairs(Integer support){

        for(Integer singleton1 : frequentSingleton){
            for(Integer singleton2 : frequentSingleton) {
                if !(frequentPairs.contains([singleton2, singleton1])) {
                    Integer[] newPair = new Integer[2]:
                    newPair[0] = singleton1;
                    newPair[1] = singleton2;
                    frequentPairs.add(newPair);
                }
            }
        }
    }

    //Generates candidates by finding all sets of size (set1.size() + 1)
    //that share the first (set1.size() - 1) items
    public static Integer[] generateCandidates(Integer[] itemset1, Integer[] itemset2) {
        int k = itemset1.length;
        Integer[] result = null;

        System.out.println(Arrays.toString(Arrays.copyOfRange(itemset1,0,k-1)));
        System.out.println(Arrays.toString(Arrays.copyOfRange(itemset2,0,k-1)));
        if (Arrays.equals(Arrays.copyOfRange(itemset1,0,k-1), Arrays.copyOfRange(itemset2,0,k-1)) && !(itemset1[k-1].equals(itemset2[k-1]))) {
            result = Arrays.copyOf(itemset1, k+1);
            System.arraycopy(itemset2,k-1,result,k,1);
        }
        return result;
    }


   public static void generateAllCandidates(List<Integer[]> size2Candidates) {
        List<Integer[]> sizeKCandidates = new ArrayList<Integer[]>();
        int currentSize = 3;
        while (currentSize <= largestTransactionSize) {

            //Creating candidates of size k by calling generate candidates





            //Call method to put into hash tree
            //Call method to count and get all positives
            //Call method to check: think about chunk size here
        }

        //After while statement
        //Create rules in here or different method

    }



	public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            //Prompts user for program details.
            //System.out.println("Type in support threshold (for count).");
            //support = Integer.parseInt(reader.readLine());
            support = 10;
            reader.close();

            //For part II
            // System.out.println("Type in confidence threshold.");
            // confidence = Integer.parseInt(reader.readLine());

            readUserData();
            readMovieNames();
            // totalTransaction = listOfSeenMoviesForEachUser.size();
            // int desiredFreq = support * totalTransaction;
            //getFrequentItemSets(support);
            Integer[] even = new Integer[4];
            even[0] = 0;
            even[1] = 2;
            even[2] = 4;
            even[3] = 6;
            Integer[] even1 = new Integer[4];
            even1[0] = 0;
            even1[1] = 2;
            even1[2] = 4;
            even1[3] = 8;
            System.out.println(Arrays.toString(generateCandidates(even, even1)));

            //Print statements for part I
            // System.out.println("Frequent movies are: ");
            // for (String key : freqMovies) {
            //     System.out.println(movieNames.get(key));
            // }
            

        } catch (IOException e) {
            e.printStackTrace();
        }

		
	}//end of main
}


//SCRATCH code
// for (Integer[] intArray : frequentPairs) {
        //     boolean end = false;
        //     int index = 0;
        //     HashMap<Integer, HashMap> currentDictionary = hashTreeForPairs;
        //     while (!end) {
        //         for (Integer item : intArray) {
        //             //If item is at the end of the array
        //             if (item.equals(intArray[intArray.length - 1])) {
        //                 HashMap<Integer, Integer> endDict = new HashMap<Integer, HashMap>();
        //                 endDict.put(item, 0)
        //                 /.HOW TO GET PREV VALUE
        //                 currentDictionary.put(item, endDict)
        //             }
        //             //If item doesn't exist in dictionary
        //             if (!currentDictionary.get(item)) {
        //                 currentDictionary.put(item, )
        //             }
        //         }

        //     }
        // }













