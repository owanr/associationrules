//Use list of arrays
//change all set to arrays

import java.io.*;
import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;

//Risako Owan and Brynna Mering

public class Apriori2 {
	private static int support;
	private static int confidence;
    //keep as set
	private static List<Set<String>> listOfSeenMoviesForEachUser = new ArrayList<Set<String>>();
	private static int totalTransaction;
    //dictionary for freq size 1 candidates
    private static HashMap<String, Integer> candidateFreqDict = new HashMap<String, Integer>();
    //Integer[] used to be Set<String>: each set of seen movies/transaction
    private static HashMap<Integer[], Integer> frequentCandidateDict = new HashMap<Integer[], Integer>();
    private static HashMap<String, String> movieNames = new HashMap<String, String>();
    private static List<Integer[]> freqMovies = new ArrayList<Integer[]>();
    private static List<Integer[]> allFreqCandidates = new ArrayList<Integer[]>();
    private static HashMap<String, HashMap> hashTree = new HashMap<String, HashMap>();
    private static int largestTransactionSize = 0;


	//Reads file and adds set of seen movies for each user to listOfSeenMoviesForEachUser
    //In the same iteration, adds a dictionary for movie frequency {movie ID : freq}
	public static void readUserData() {
		try {
            File file = new File("ratings.dat");
            Scanner scanner = new Scanner(file);

            //Assumes scanner has at least one line
            String[] fileLine = scanner.nextLine().split("::");

            //Set of strings or set of integers?
            Set<String> movieSet = new HashSet<String>();
            movieSet.add(fileLine[1]);

            String currentUser = fileLine[0];

            
            //While nextLine isn't null
            while (scanner.hasNextLine()) {
            	
            	fileLine = scanner.nextLine().split("::");

            	//Adding movies to each user's set of viewed movies
                //If we are at the next user
            	if (!currentUser.equals(fileLine[0])) {
                    
                    if (movieSet.size() > largestTransactionSize) {
                        largestTransactionSize = movieSet.size();

                    }

                    //Array or list?????
            		listOfSeenMoviesForEachUser.add(movieSet);
            		movieSet = new HashSet<String>();
            		currentUser = fileLine[0];
            	}
            	
            	movieSet.add(fileLine[1]);

                
                //Adding to dictionary of movie frequency
                //Helps create frequent candidates of size one
                if(candidateFreqDict.containsKey(fileLine[1])){
                    int old = candidateFreqDict.get(fileLine[1]);
                    candidateFreqDict.put(fileLine[1], old + 1);
                }else{
                    candidateFreqDict.put(fileLine[1], 1);
                }
            }

            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
	}

    //Reads in data from movies.dat and creates dictionary {ID: movie names}
    public static void readMovieNames(){

        try {
            File file = new File("movies.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String[] fileLine;
            String tempFileLine = reader.readLine();
            while (tempFileLine != null) {
                fileLine = tempFileLine.split("::");
                movieNames.put(fileLine[0], fileLine[1]);
                tempFileLine = reader.readLine();
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Iterates through candidateFreqDict and creates a set (freqMovies) of frequent movies 
    //according to the given support threshold
    public static void getFrequentItemSets(){

        for(String key : candidateFreqDict.keySet()){
            if(candidateFreqDict.get(key)>=support){
                Integer[] newCandidateArray = new Integer[1];
                newCandidateArray[0] = Integer.parseInt(key);
                freqMovies.add(newCandidateArray);
            }
        }
    }

    //Generates candidates by finding all sets of size (set1.size() + 1)
    //that share the first (set1.size() - 1) items
    public static Integer[] generateCandidates(Integer[] itemset1, Integer[] itemset2) {
        int k = itemset1.length + 1;
        Integer[] result = null;

        // System.out.println(Arrays.toString(Arrays.copyOfRange(itemset1,0,k-2)));
        // System.out.println(Arrays.toString(Arrays.copyOfRange(itemset2,0,k-2)));

        Integer[] firstPartOfItemset1 = Arrays.copyOfRange(itemset1,0,k-2);
        Integer[] firstPartOfItemset2 = Arrays.copyOfRange(itemset2,0,k-2);

        //If every element in the first k-2 are the same and the very last are different
        // System.out.println(Arrays.toString(firstPartOfItemset1) + " = " + Arrays.toString(firstPartOfItemset2));
        // System.out.println(itemset1[k-2] + " != " + itemset2[k-2]);

        if (Arrays.equals(firstPartOfItemset1, firstPartOfItemset2) && !(itemset1[k-2].equals(itemset2[k-2]))) {
            result = Arrays.copyOf(itemset1, k);
            System.arraycopy(itemset2,k-2,result,k-1,1);
            //System.out.println("here"+Arrays.toString(result));
        }
        //System.out.println("hereeeeeee"+Arrays.toString(result));
        return result;
    }//words


    public static void generateAllCandidates() {
        List<Integer[]> kMinus1ItemSets = new ArrayList<Integer[]>();
        getFrequentItemSets();
        List<Integer[]> sizeKItemSets = new ArrayList<Integer[]>();
        kMinus1ItemSets =  freqMovies;   //size 3260 
        //currentSize represents k-1
        int currentSize = 1;
        while (currentSize <= 10) {
            //Creating candidates of size k by calling generate candidates
            //System.out.println("new while loop starting here");
            for(int i = 0; i < currentSize; i++){
                for(int j = 0; j < currentSize; j++){
                    // System.out.println("at iteration i: " + i + ", j: "+ j);
                    // System.out.println("hello"+Arrays.toString(kMinus1ItemSets.get(i)));
                    // System.out.println("goodbye"+Arrays.toString(kMinus1ItemSets.get(j)));
                    Integer[] newCandidate = generateCandidates(kMinus1ItemSets.get(i), kMinus1ItemSets.get(j));
                    // System.out.println("nc1"+generateCandidates(kMinus1ItemSets.get(i), kMinus1ItemSets.get(j)));
                    // System.out.println("nc2"+newCandidate);
                    if (newCandidate != null) {
                        Arrays.sort(newCandidate);

                        if(!sizeKItemSets.contains(newCandidate)){
                            sizeKItemSets.add(newCandidate);
                        }
                    }
                
                }
            }
            currentSize++;
            //build hash
            //take count in hash
            //check with original
            if (currentSize != 10) {
                System.out.println(sizeKItemSets.size() + "*");
                allFreqCandidates.addAll(sizeKItemSets);
                kMinus1ItemSets.clear();
                kMinus1ItemSets.addAll(sizeKItemSets);
                System.out.println(kMinus1ItemSets.size() + "***");
                sizeKItemSets = new ArrayList<Integer[]>();
            }
            
        

            //Call method to put into hash tree
            //Call method to count and get all positives
            //Call method to check: think about chunk size here

        }//end of while loop

        for (int i = 0; i < sizeKItemSets.size(); i++) {
            System.out.println(Arrays.toString(sizeKItemSets.get(i)));
        }
        

        //After while statement
        //Create rules in here or different method

    }



	public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            //Prompts user for program details.
            //System.out.println("Type in support threshold (for count).");
            //support = Integer.parseInt(reader.readLine());
            support = 10;
            reader.close();

            //For part II
            // System.out.println("Type in confidence threshold.");
            // confidence = Integer.parseInt(reader.readLine());

            readUserData();
            readMovieNames();
            // totalTransaction = listOfSeenMoviesForEachUser.size();
            // int desiredFreq = support * totalTransaction;
            //getFrequentItemSets(support);
            Integer[] even = new Integer[4];
            even[0] = 0;
            even[1] = 2;
            even[2] = 4;
            even[3] = 6;
            Integer[] even1 = new Integer[4];
            even1[0] = 0;
            even1[1] = 2;
            even1[2] = 4;
            even1[3] = 8;
            System.out.println(Arrays.toString(generateCandidates(even, even1)));
            generateAllCandidates();
            //Print statements for part I
            // System.out.println("Frequent movies are: ");
            // for (String key : freqMovies) {
            //     System.out.println(movieNames.get(key));
            // }
            

        } catch (IOException e) {
            e.printStackTrace();
        }

		
	}//end of main
}