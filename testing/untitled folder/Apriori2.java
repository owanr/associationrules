import java.io.*;
import java.util.Scanner;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;

//Risako Owan and Brynna Mering


public class Apriori {
	private static int support;
	private static int confidence;
	private static List<Set<String>> listOfSeenMoviesForEachUser = new ArrayList<Set<String>>();
	private static int totalTransaction;
    private static HashMap<String, Integer> candidateFreqDict = new HashMap<String, Integer>();
    private static HashMap<String, String> movieNames = new HashMap<String, String>();
    private static Set<String> freqMovies = new HashSet<String>();

	//Reads file and adds set of seen movies for each user to listOfSeenMoviesForEachUser
	public static void readUserData() {
		try {
            File file = new File("ratings.dat");
            Scanner scanner = new Scanner(file);

            //Assumes scanner has at least one line
            String[] fileLine = scanner.nextLine().split("::");

            //Set of strings or set of integers?
            Set<String> movieSet = new HashSet<String>();
            movieSet.add(fileLine[1]);

            String currentUser = fileLine[0];

            
            //While nextLine isn't null
            while (scanner.hasNextLine()) {
            	
            	fileLine = scanner.nextLine().split("::");

            	//Adding movies to each user's set of viewed movies
                //If we are at the next user
            	if (!currentUser.equals(fileLine[0])) {
            		listOfSeenMoviesForEachUser.add(movieSet);
            		movieSet = new HashSet<String>();
            		currentUser = fileLine[0];
            	}
            	
            	movieSet.add(fileLine[1]);

                
                //adding to dictionary of movie frequency
                if(candidateFreqDict.containsKey(fileLine[1])){
                    int old = candidateFreqDict.get(fileLine[1]);
                    candidateFreqDict.put(fileLine[1], old + 1);
                }else{
                    candidateFreqDict.put(fileLine[1], 1);
                }
            }

            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
	}

    public static void readMovieNames(){

        try {
            File file = new File("movies.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String[] fileLine;
            String tempFileLine = reader.readLine();
            while (tempFileLine != null) {
                fileLine = tempFileLine.split("::");
                movieNames.put(fileLine[0], fileLine[1]);
                tempFileLine = reader.readLine();
            }

            //Broken code: scanner didn't work. Why?
            // Scanner scanner = new Scanner(file);

            // //Assumes scanner has at least one line
            // String[] fileLine = {};

            // while (scanner.hasNextLine()) {
            //     fileLine = scanner.nextLine().split("::");
            //     movieNames.put(fileLine[0], fileLine[1]);
            //     //System.out.println(fileLine[1]);
            // }
            // // System.out.println("--------------------------");
            // // System.out.println(scanner.nextLine());

            // // System.out.println("fileLine[0 is" + fileLine[1]);

            // scanner.close();
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getFrequentItemSets(Integer support){
        for(String key : candidateFreqDict.keySet()){
            if(candidateFreqDict.get(key)>=support){
                freqMovies.add(key);
            }
        }
    }



	public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            //Prompts user for program details.
            System.out.println("Type in support threshold.");
            support = Integer.parseInt(reader.readLine());
            reader.close();

            //For part II
            // System.out.println("Type in confidence threshold.");
            // confidence = Integer.parseInt(reader.readLine());

            readUserData();
            readMovieNames();
            // totalTransaction = listOfSeenMoviesForEachUser.size();
            // int desiredFreq = support * totalTransaction;
            getFrequentItemSets(support);

            //Print statements for part I
            System.out.println("Frequent movies are: ");
            for (String key : freqMovies) {
                System.out.println(movieNames.get(key));
            }
            

        } catch (IOException e) {
            e.printStackTrace();
        }

		
	}//end of main
}