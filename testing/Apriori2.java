//Use list of arrays
//change all set to arrays

import java.io.*;
import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;

//Risako Owan and Brynna Mering

public class Apriori2 {
	private static int support;
	private static int confidence;
    //keep as set
	private static List<Set<String>> listOfSeenMoviesForEachUser = new ArrayList<Set<String>>();
	private static int totalTransaction;
    //dictionary for freq size 1 candidates
    private static HashMap<String, Integer> candidateFreqDict = new HashMap<String, Integer>();
    //Integer[] used to be Set<String>: each set of seen movies/transaction
    private static HashMap<Integer[], Integer> frequentCandidateDict = new HashMap<Integer[], Integer>();
    private static HashMap<String, String> movieNames = new HashMap<String, String>();
    
    
    private static List<Integer[]> allFrequentCandidates = new ArrayList<Integer[]>();
    private static HashMap<String, HashMap> hashTree = new HashMap<String, HashMap>();
    private static int largestTransactionSize = 0;


	//Reads file and adds set of seen movies for each user to listOfSeenMoviesForEachUser
    //In the same iteration, adds a dictionary for movie frequency {movie ID : freq}
	public static void readUserData() {
		try {
            File file = new File("ratings.dat");
            Scanner scanner = new Scanner(file);

            //Assumes scanner has at least one line
            String[] fileLine = scanner.nextLine().split("::");

            //Set of strings or set of integers?
            Set<String> movieSet = new HashSet<String>();
            movieSet.add(fileLine[1]);

            String currentUser = fileLine[0];

            
            //While nextLine isn't null
            while (scanner.hasNextLine()) {
            	
            	fileLine = scanner.nextLine().split("::");

            	//Adding movies to each user's set of viewed movies
                //If we are at the next user
            	if (!currentUser.equals(fileLine[0])) {
                    
                    if (movieSet.size() > largestTransactionSize) {
                        largestTransactionSize = movieSet.size();

                    }

                    //Array or list?????
            		listOfSeenMoviesForEachUser.add(movieSet);
            		movieSet = new HashSet<String>();
            		currentUser = fileLine[0];
            	}
            	
            	movieSet.add(fileLine[1]);

                
                //Adding to dictionary of movie frequency
                //Helps create frequent candidates of size one
                if(candidateFreqDict.containsKey(fileLine[1])){
                    int old = candidateFreqDict.get(fileLine[1]);
                    candidateFreqDict.put(fileLine[1], old + 1);
                }else{
                    candidateFreqDict.put(fileLine[1], 1);
                }
            }

            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
	}

    //Reads in data from movies.dat and creates dictionary {ID: movie names}
    public static void readMovieNames(){

        try {
            File file = new File("movies.dat");
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String[] fileLine;
            String tempFileLine = reader.readLine();
            while (tempFileLine != null) {
                fileLine = tempFileLine.split("::");
                movieNames.put(fileLine[0], fileLine[1]);
                tempFileLine = reader.readLine();
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Iterates through candidateFreqDict and creates a set (frequentSingleton) of freq. singletons 
    //according to the given support threshold
    public static Set<Integer> getFrequentSingletons(){
        Set<Integer> frequentSingleton = new HashSet<Integer>();

        for(String key : candidateFreqDict.keySet()){
            if(candidateFreqDict.get(key)>=support){
                frequentSingleton.add(Integer.parseInt(key));
            }
        }
        return frequentSingleton;
    }

    //Iterates through candidateFreqDict and creates a list of arrays of freq. pairs 
    //according to the given support threshold
    public static List<Integer[]> getFrequentPairs(Set<Integer> frequentSingleton){
        List<Integer[]> frequentPairs = new ArrayList<Integer[]>();
        
        //Creates a list of non-duplicate pairs
        for(Integer singleton1 : frequentSingleton){
            for(Integer singleton2 : frequentSingleton) {

                if (!(singleton1.equals(singleton2))) {

                    Integer[] newPair = new Integer[2];
                    newPair[0] = singleton1;
                    newPair[1] = singleton2;
                    Arrays.sort(newPair);

                    if (!(frequentPairs.contains(newPair))) {
                        frequentPairs.add(newPair);
                    }
                }
                
            }
        }



        return frequentPairs;
    }

    public static List<Integer[]> createTripletCandidates(List<Integer[]> frequentPairs){
        List<Integer[]> tripletCandidates = new ArrayList<Integer[]>();

        int listSize = frequentPairs.size();

        for(int i = 0; i < listSize; i++){
            for(int j = 0; j < listSize; j++){
                if (i != j) {
                    Integer[] newCandidate = generateCandidates(frequentPairs.get(i), frequentPairs.get(j));
                    
                    if (newCandidate != null) {
                        Arrays.sort(newCandidate);

                        if(!tripletCandidates.contains(newCandidate)){
                            tripletCandidates.add(newCandidate);
                        }
                    }
                }
            }
        }

        return tripletCandidates;
    }//end of createTripletCandidates


    // public static void ahhhh() {

    //     //Creates hash tree with initial count 0
    //     HashMap<Integer, HashMap> hashTreeForPairs = new HashMap<Integer, HashMap>();
    //     for (Integer[] frequentPairArray : frequentPairs) {
    //         Integer[] newArray = makeSubarrayCopy(frequentPairArray);
    //         hashTreeForPairs.put(frequentPairArray[0], recursiveHashTree(newArray));
    //     }


        //go through each transaction and increment count
        //recursively go down and figure out frequent pairs

        return frequentPairs;
    }//end of getFrequentPairs

    //The length of frequentItem must be at least 2
    public static HashMap<Integer,HashMap> recursiveHashTree(Integer[] frequentItem) {
        System.out.println("entered recursive with" + Arrays.toString(frequentItem));
        //base case
        if (frequentItem.length == 2) {
            HashMap<Integer,Integer> lastDict = new HashMap<Integer,Integer>();
            lastDict.put(frequentItem[1], 0);
            HashMap<Integer,HashMap> dict1 = new HashMap<Integer,HashMap>();
            dict1.put(frequentItem[0],lastDict);
            return dict1;
        //recursive case
        } else {
            HashMap<Integer,HashMap> dict2 = new HashMap<Integer,HashMap>();
            Integer[] newArray = makeSubarrayCopy(frequentItem);
            //Arrays.copyOfRange(frequentItem, 1, frequentItem.length);
            dict2.put(frequentItem[0], recursiveHashTree(newArray));
            return dict2;
        }

    }

    public static Integer[] makeSubarrayCopy(Integer[] originalArray) {
        Integer[] temp = new Integer[originalArray.length - 1];
        for (int i = 1; i < originalArray.length; i++) {
            //Assumes no nulls
            temp[i-1] = originalArray[i];
        }
        return temp;
    }
    //------------
    // //Generates candidates by finding all sets of size (set1.size() + 1)
    // //that share the first (set1.size() - 1) items
    public static Integer[] generateCandidates(Integer[] itemset1, Integer[] itemset2) {
        int k = itemset1.length + 1;
        Integer[] result = null;

        // System.out.println(Arrays.toString(Arrays.copyOfRange(itemset1,0,k-2)));
        // System.out.println(Arrays.toString(Arrays.copyOfRange(itemset2,0,k-2)));

        Integer[] firstPartOfItemset1 = Arrays.copyOfRange(itemset1,0,k-2);
        Integer[] firstPartOfItemset2 = Arrays.copyOfRange(itemset2,0,k-2);

        //If every element in the first k-2 are the same and the very last are different
        // System.out.println(Arrays.toString(firstPartOfItemset1) + " = " + Arrays.toString(firstPartOfItemset2));
        // System.out.println(itemset1[k-2] + " != " + itemset2[k-2]);

        if (Arrays.equals(firstPartOfItemset1, firstPartOfItemset2) && !(itemset1[k-2].equals(itemset2[k-2]))) {
            result = Arrays.copyOf(itemset1, k);
            System.arraycopy(itemset2,k-2,result,k-1,1);
            //System.out.println("here"+Arrays.toString(result));
        }
        //System.out.println("hereeeeeee"+Arrays.toString(result));
        return result;
    }//words


    // public static void generateAllCandidates() {
    //     List<Integer[]> kMinus1ItemSets = new ArrayList<Integer[]>();
    //     getFrequentItemSets();
    //     List<Integer[]> sizeKItemSets = new ArrayList<Integer[]>();
    //     kMinus1ItemSets =  freqMovies;   //size 3260 

    //     int currentSize = 1; //= k-1
    //     while (currentSize <= 10) {
    //         //Creating candidates of size k by calling generate candidates

    //         for(int i = 0; i < currentSize; i++){
    //             for(int j = 0; j < currentSize; j++){

    //                 Integer[] newCandidate = generateCandidates(kMinus1ItemSets.get(i), kMinus1ItemSets.get(j));
    //                 if (newCandidate != null) {
    //                     Arrays.sort(newCandidate);

    //                     if(!sizeKItemSets.contains(newCandidate)){
    //                         sizeKItemSets.add(newCandidate);
    //                     }
    //                 }
                
    //             }
    //         }
    //         currentSize++;
    //         //build hash
    //         //take count in hash
    //         //check with original
    //         if (currentSize != 10) {
    //             System.out.println(sizeKItemSets.size() + "*");
    //             allFreqCandidates.addAll(sizeKItemSets);
    //             kMinus1ItemSets.clear();
    //             kMinus1ItemSets.addAll(sizeKItemSets);
    //             System.out.println(kMinus1ItemSets.size() + "***");
    //             sizeKItemSets = new ArrayList<Integer[]>();
    //         }
            
        

    //         //Call method to put into hash tree
    //         //Call method to count and get all positives
    //         //Call method to check: think about chunk size here

    //     }//end of while loop

    //     for (int i = 0; i < sizeKItemSets.size(); i++) {
    //         System.out.println(Arrays.toString(sizeKItemSets.get(i)));
    //     }
        

    //     //After while statement
    //     //Create rules in here or different method

    // }//end of generateAllCandidates()



	public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            //Prompts user for program details.
            //System.out.println("Type in support threshold (for count).");
            //support = Integer.parseInt(reader.readLine());
            support = 1000;
            reader.close();

            //For part II
            // System.out.println("Type in confidence threshold.");
            // confidence = Integer.parseInt(reader.readLine());

            readUserData();
            readMovieNames();
            // totalTransaction = listOfSeenMoviesForEachUser.size();
            // int desiredFreq = support * totalTransaction;
            //getFrequentItemSets(support);
            // Integer[] even = new Integer[4];
            // even[0] = 0;
            // even[1] = 2;
            // even[2] = 4;
            // even[3] = 6;
            // Integer[] even1 = new Integer[4];
            // even1[0] = 0;
            // even1[1] = 2;
            // even1[2] = 4;
            // even1[3] = 8;
            // System.out.println(Arrays.toString(generateCandidates(even, even1)));
            // generateAllCandidates();
            Set<Integer> frequentSingleton = getFrequentSingletons();
            System.out.println("num of singletons: "+frequentSingleton.size());
            List<Integer[]> frequentPairs = getFrequentPairs(frequentSingleton);
            System.out.println("all pairs of singletons: "+(frequentSingleton.size()*frequentSingleton.size()));
            System.out.println("frequent pairs: "+(frequentPairs.size()));
            List<Integer[]> triple = getFrequentPairs(frequentSingleton);
            // for (int i = 0; i < frequentPairs.size(); i++) {
            //     System.out.println(Arrays.toString(frequentPairs.get(i)));
            // }
            // Integer[] test = {1,2,3};
            // HashMap<Integer,HashMap> testDict = recursiveHashTree(test);
            // System.out.println(testDict);

            // for (int i : testDict.keySet()) {
            //     while (!testDict.get(i).getClass().equals(int.class)) {
            //         System.out.println("key: " + i + ", val: "+testDict.get(i));
            //     }
            // }

        } catch (IOException e) {
            e.printStackTrace();
        }

		
	}//end of main
}